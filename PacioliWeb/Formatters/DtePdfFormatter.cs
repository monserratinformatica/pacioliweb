﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using PacioliWeb.Models;

namespace PacioliWeb.Formatters
{
    public class DtePdfFormatter : BufferedMediaTypeFormatter
    {
        public DtePdfFormatter()
        {
            // Add the supported media type.
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/pdf"));
        }

        public override bool CanWriteType(System.Type type)
        {
            return true;            
        }

        public override bool CanReadType(Type type)
        {
            return false;
        }

        public override void WriteToStream(Type type, object value, Stream writeStream, HttpContent content)
        {
            
        }

    }
}