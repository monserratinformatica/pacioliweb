﻿using PacioliWeb.Areas.HelpPage.ModelDescriptions;
using PacioliWeb.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PacioliWeb.Models
{
    [ModelName("PacioliImpuesto")]
    public class PacioliImpuesto
    {
        [Required]
        public int Codigo
        {
            get;
            set;
        }
        [Required]
        public double Tasa
        {
            get;
            set;
        }
    }
    [ModelName("PacioliImpuestos")]
    public class PacioliImpuestos : List<PacioliImpuesto> { };


    [ModelName("PacioliCodigoDetalle")]
    public class PacioliCodigoDetalle
    {
        [Required]
        public string TipoCodigo
        {
            get;
            set;
        }
        [Required]
        public string ValorCodigo
        {
            get;
            set;
        }
    }

    [ModelName("PacioliCodigosDetalle")]
    public class PacioliCodigosDetalle : List<PacioliCodigoDetalle> { };

    /// <summary>
    /// Línea de Detalle
    /// </summary>
    [ModelName("PacioliLineaDetalle")]
    public class PacioliLineaDetalle
    {
        [Required]
        public double Cantidad
        {
            get;
            set;
        }

        public bool Exento
        {
            get;
            set;
        }

        public PacioliImpuestos Impuestos { get; set; } = new PacioliImpuestos();

        public PacioliCodigosDetalle Codigos { get; set; } = new PacioliCodigosDetalle();

        public decimal Monto
        {
            get;
            set;
        }

        [Required]
        public string NombreItem
        {
            get;
            set;
        }

        public string DescripcionItem
        {
            get;
            set;
        }

        public double PorcentajeDescuento
        {
            get;
            set;
        }

        [Required]
        public decimal Precio
        {
            get;
            set;
        }

        public string TipoCodigo
        {
            get;
            set;
        }

        public string UnidadMedida
        {
            get;
            set;
        }

        public string ValorCodigo
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Listado con lineas de detalle
    /// </summary>
    [ModelName("PacioliDetalle")]
    [CollectionDataContract(Name = "PacioliLineaDetalle", Namespace = "")]
    public class PacioliDetalle : List<PacioliLineaDetalle>, IList<PacioliLineaDetalle> { }

    /// <summary>
    /// Listado de Documentos de Referencia
    /// </summary>
    [ModelName("PacioliDocumentosReferencia")]
    public class PacioliDocumentosReferencia : List<PacioliDocumentoReferencia> { }

    /// <summary>
    /// Datos para una empresa
    /// </summary>
    [ModelName("PacioliEmpresa")]
    public class PacioliEmpresa
    {
        [Required]
#pragma warning disable 1591
        public string Ciudad
        {
            get;
            set;
        }
        [Required]
        public string Comuna
        {
            get;
            set;
        }
        [Required]
        public string Direccion
        {
            get;
            set;
        }
#pragma warning restore 1591
        /// <summary>
        /// Giro del contribuyente tal como está registrado en el SII
        /// </summary>
        [Required]
        public string Giro
        {
            get;
            set;
        }
        /// <summary>
        /// Parte numérica del RUT del contribuyente
        /// </summary>
        [Required]
        [Range(1000000, 99999999)]
        public int RUT
        {
            get;
            set;
        }
        [Required]
        public string RazonSocial
        {
            get;
            set;
        }

        public string Telefono
        {
            get;
            set;
        }

        public string EmailIntercambio
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Referencia a otro documento
    /// </summary>
    [ModelName("PacioliDocumentoReferencia")]
    public class PacioliDocumentoReferencia
    {
        [Required]
        public string TipoDocumento { get; set; }
        [Required]
        public string Folio { get; set; }
        [Required]
        public DateTime FechaReferencia { get; set; }
        [Required]
        public string RazonReferencia { get; set; }
    }

    [ModelName("PacioliDTE")]
    public class PacioliDTE
    {
        PacioliEmpresa emisor;
        PacioliEmpresa receptor = new PacioliEmpresa();

        /// <summary>
        /// 
        /// </summary>
        /// <see cref="PacioliDetalle"/>
        public PacioliDetalle Detalle { get; set; } = new PacioliDetalle();

        public DateTime Fecha
        {
            get;
            set;
        }

        public string FormaDePago
        {
            get;
            set;
        }

        public string Comentarios
        {
            get;
            set;
        }

        public PacioliEmpresa Emisor
        {
            get { return emisor; }
            set { emisor = value; }
        }


        [Required(ErrorMessage = "El receptor de una factura debe estar definido")]
        public PacioliEmpresa Receptor
        {
            get { return receptor; }
            set { receptor = value; }
        }


        public PacioliDocumentosReferencia DocumentosReferencia { get; set; } = new PacioliDocumentosReferencia();
    }

    [ModelName("PacioliFacturaElectronica")]
    public class PacioliFacturaElectronica : PacioliDTE
    {

    }

    [ModelName("PacioliNotaCreditoElectronica")]
    public class PacioliNotaCreditoElectronica : PacioliDTE
    {
        [Required]
        public int FolioRef
        {
            get;
            set;
        }

        [Required]
        [TipoDocumentoValido]
        public int TipoDocRef
        {
            get;
            set;
        }

        [Required]
        public DateTime FechaReferencia
        {
            get;
            set;
        }

        /// <summary>
        /// Motivo de la nota de crédito.
        /// </summary>
        /// <list>
        ///     <item><term>1</term><description>Anula documento de referencia</description></item>
        ///     <item><term>2</term><description>Corrige Texto Documento de Referencia</description></item>
        ///     <item><term>3</term><description>Corrige montos</description></item>
        /// </list>
        /// <seealso href="http://www.sii.cl/factura_electronica/factura_mercado/formato_dte.pdf">Formato Documentos Electrónicos</seealso>
        [Required]
        [Range(1, 3)]
        public int MotivoNota
        {
            get;
            set;
        }
    }

    [ModelName("PacioliNotaDebitoElectronica")]
    public class PacioliNotaDebitoElectronica : PacioliNotaCreditoElectronica
    {

    }

    [ModelName("PacioliFacturaExentaElectronica")]
    public class PacioliFacturaExentaElectronica : PacioliDTE
    {

    }

    [ModelName("PacioliFacturaCompraElectronica")]
    public class PacioliFacturaCompraElectronica : PacioliDTE
    {

    }

    [ModelName("PacioliGuiaDespachoElectronica")]
    public class PacioliGuiaDespachoElectronica : PacioliDTE
    {
        [Required]
        [Range(1, 9)]
        public int MotivoGuia
        {
            get;
            set;
        }

        public int TipoDespacho { get; set; }
    }

    [ModelName("PacioliEstadoDTE")]
    public class PacioliEstadoDTE
    {

        public string RawResponse { get; set; }
        public int ErrCode { get; set; }
        public string NumAtencion { get; set; }
        public string GlosaErr { get; set; }
        public string GlosaEstado { get; set; }
        public string Estado { get; set; }

    }

    [ModelName("PacioliResumenDocumento")]
    public class PacioliResumenDocumento
    {
        public int Id { get; set; }
        public string TipoDocumento { get; set; }
        public long Folio { get; set; }
        public int RutEmisor { get; set; }
        public DateTime FechaEmision { get; set; }
        public decimal Total { get; set; }
        public decimal Iva { get; set; }
    }

    [ModelName("PacioliListadoDocumentos")]
    public class PacioliListadoDocumentos : List<PacioliResumenDocumento> { }

    [ModelName("PacioliTipoDocumento")]
    public static class PacioliTipoDocumento
    {
        public const int FacturaElectronica = 33, FacturaExentaElectronica = 34, NotaCreditoElectronica = 61, NotaDebitoElectronica = 56, GuiaDespachoElectronica = 52, FacturaCompraElectronica = 46;
    }
}