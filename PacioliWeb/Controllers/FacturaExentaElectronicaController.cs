﻿using PacioliSrv;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Net.Http.Headers;
using System.IO;
using PacioliWeb.Models;
using Pacioli.Filters;

namespace PacioliWeb.Controllers
{

    public class FacturaExentaElectronicaController : DTEController
    {
        
        public IHttpActionResult GetTest()
        {
            var factura = new PacioliFacturaExentaElectronica();
            factura.Receptor.RazonSocial = "Empresa";
            factura.Receptor.Direccion = "dirección";
            factura.Receptor.Comuna = "comuna";
            factura.Receptor.Ciudad = "ciudad";
            factura.Receptor.Giro = "girillo";
            factura.Receptor.RUT = 76204893;

            var d = new PacioliLineaDetalle();
            d.Exento = true;
            d.Cantidad = 1;
            d.Precio = 100;
            d.Monto = 100;
            d.UnidadMedida = "cajas";
            d.ValorCodigo = "123";
            d.TipoCodigo = "Interno";
            factura.Detalle.Add(d);

            return Ok<PacioliFacturaExentaElectronica>(factura);
        }

        [ValidateModel]
        public IHttpActionResult Post(PacioliFacturaExentaElectronica Doc)
        {
            string mensaje;
            var obj = ComPacioliSingleton.GetPacioliSrv(out mensaje);
            if (obj == null)
                return InternalServerError();
            
            if (Doc == null || Doc.Detalle.Count == 0)
                return BadRequest();

            IPacioliFacturaExentaElectronica dte = obj.NuevaFacturaExentaElectronica();

            AgregarEncabezado(Doc, dte);

            AgregarLineasDetalle(Doc, dte);

            AgregarDocumentosReferencia(Doc, dte);

            obj.EmitirDTE(dte, out mensaje);
            if (dte.Folio > 0)
            {
                var res = new ResultadoEnvio()
                {
                    Id = dte.NumeroEnvio,
                    Folio = dte.Folio
                };
                return Ok<ResultadoEnvio>(res);
            }
            return BadRequest(mensaje);
        }

        public IHttpActionResult GetDTE(int TipoDTE, int Folio)
        {
            
            return NotFound();
        }

        [Route("api/facturaexentaelectronica/{Folio}/estado")]
        [HttpGet]
        public IHttpActionResult Estado(int Folio)
        {
            return this.Estado(PacioliTipoDocumento.FacturaExentaElectronica, Folio);

        }
    }
}
