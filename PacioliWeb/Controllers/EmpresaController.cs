﻿using PacioliSrv;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PacioliWeb.Models;

namespace PacioliWeb.Controllers
{
    public class EmpresaController : ApiController
    {
        /// <summary>
        /// Obtiene datos de una empresa incluyendo email de intercambio de una empresa por RUT
        /// </summary>
        /// <param name="rut">Rut de la empresa emisora</param>        
        /// <returns>Objeto PacioliEmpresa</returns>
        [Route("api/empresa/{Rut}")]
        [HttpGet]
        public IHttpActionResult Get(int rut)
        {
            string msg;
            var obj = ComPacioliSingleton.GetPacioliSrv(out msg);
            string mensaje;
            var emisor = obj.ObtenerEmisor(rut, out mensaje);


            if (emisor == null && mensaje == null)
            {
                return NotFound();
            }
            if (mensaje != null)
            {
                return BadRequest(mensaje);
            }

            PacioliEmpresa pacioliEmpresa = new PacioliEmpresa();
            pacioliEmpresa.RUT = emisor.Rut;
            pacioliEmpresa.RazonSocial = emisor.RazonSocial;            
            pacioliEmpresa.Telefono = emisor.Telefono;            
            pacioliEmpresa.Giro = emisor.Giro;
            pacioliEmpresa.EmailIntercambio = emisor.EmailIntercambio;
            return Ok(pacioliEmpresa);
        }
    }
}
