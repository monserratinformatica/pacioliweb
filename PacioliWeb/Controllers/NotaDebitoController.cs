﻿using Pacioli.Filters;
using PacioliSrv;
using PacioliWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PacioliWeb.Controllers
{
    public class NotaDebitoController : DTEController
    {
        // GET: api/NotaDebito
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// Obtiene Nota de Débito electrónica emitida. El tipo de documento obtenido es determinado por el request.
        /// es un shortcut al Get del controller DTE        
        /// </summary>       
        /// <param name="folio">Número de folio del documento</param>
        /// <param name="rutEmisor">Rut del emisor del documento</param>
        /// <returns>Dte</returns> 
        [Route("api/notadebito/{folio}/{rutEmisor?}")]
        [HttpGet]
        public HttpResponseMessage Get(int folio, int rutEmisor = -1)
        {
            return this.Get(PacioliTipoDocumento.NotaDebitoElectronica, folio, rutEmisor);
        }

        /// <summary>Permite emitir una nota de débito</summary>
        /// <param name="Doc">Nota de débito a generar.</param>
        /// <remarks>FolioRef y TipoDocRef deben ser valores correspondientes a un documento ya emitido
        /// por Pacioli anteriormente, pues estos serán usados para obtener el documento original de la base de datos.</remarks>
        // POST: api/NotaDebito
        [ValidateModel]
        public IHttpActionResult Post(PacioliNotaDebitoElectronica Doc)
        {
            if (Doc == null)
                return BadRequest("No se pudo obtener documento desde request (JSON)");
            string msg;
            var obj = ComPacioliSingleton.GetPacioliSrv(out msg);
            var dte = obj.NuevaNotaDebitoElectronica();
            AgregarEncabezado(Doc, dte);
            AgregarLineasDetalle(Doc, dte);
            AgregarDocumentosReferencia(Doc, dte);

            string mensaje;
            obj.EmitirDTE(dte, out mensaje);
            if (dte.Folio > 0)
            {
                var res = new ResultadoEnvio()
                {
                    Id = dte.NumeroEnvio,
                    Folio = dte.Folio
                };
                return Ok<ResultadoEnvio>(res);
            }
            return BadRequest(mensaje);
        }

        [Route("api/notadebitoelectronica/{Folio}/estado")]
        [HttpGet]
        public IHttpActionResult Estado(long Folio)
        {
            return this.Estado(PacioliTipoDocumento.NotaDebitoElectronica, Folio);

        }

        protected override void AgregarEncabezado(PacioliDTE Doc, IPacioliDTE dte)
        {
            base.AgregarEncabezado(Doc, dte);

            if (dte is IPacioliNotaDebitoElectronica)
            {
                ((IPacioliNotaDebitoElectronica)dte).FolioRef = ((PacioliNotaDebitoElectronica)Doc).FolioRef;
                ((IPacioliNotaDebitoElectronica)dte).MotivoNota = ((PacioliNotaDebitoElectronica)Doc).MotivoNota;
                ((IPacioliNotaDebitoElectronica)dte).TipoDocRef = ((PacioliNotaDebitoElectronica)Doc).TipoDocRef;
            }

        }
    }
}
