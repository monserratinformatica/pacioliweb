﻿using PacioliSrv;
using System.Net.Http;
using System.Web.Http;
using PacioliWeb.Models;
using Pacioli.Filters;

namespace PacioliWeb.Controllers
{

    public class GuiaDespachoElectronicaController : DTEController
    {

        /// <summary>
        /// Obtiene Nota de Credito electrónica emitida. El tipo de documento obtenido es determinado por el request.
        /// es un shortcut al Get del controller DTE        
        /// </summary>       
        /// <param name="folio">Número de folio del documento</param>
        /// <param name="rutEmisor">Rut del emisor del documento</param>
        /// <returns>Dte</returns> 
        [Route("api/guiadespachoelectronica/{folio}/{rutEmisor?}")]
        [HttpGet]
        public HttpResponseMessage Get(int folio, int rutEmisor = -1)
        {
            return Get(PacioliTipoDocumento.GuiaDespachoElectronica, folio, rutEmisor);
        }

        [ValidateModel]
        public IHttpActionResult Post(PacioliGuiaDespachoElectronica guia)
        {
            
            if (guia == null || guia.Detalle.Count == 0)
                return BadRequest();
            string mensaje;
            var obj = ComPacioliSingleton.GetPacioliSrv(out mensaje);
            if (obj == null)
                return InternalServerError();
            var dte = obj.NuevaGuiaDespachoElectronica();

            AgregarEncabezado(guia, dte);

            AgregarLineasDetalle(guia, dte);

            AgregarDocumentosReferencia(guia, dte);

            obj.EmitirDTE(dte, out mensaje);
            if (dte.Folio <= 0) return BadRequest(mensaje);
            var res = new ResultadoEnvio()
            {
                Id = dte.NumeroEnvio,
                Folio = dte.Folio
            };
            return Ok(res);
        }  

        protected override void AgregarEncabezado(PacioliDTE Doc, IPacioliDTE dte) 
        {
            base.AgregarEncabezado(Doc, dte);

            var pacioliGuiaDespachoElectronica = dte as IPacioliGuiaDespachoElectronica;
            if (pacioliGuiaDespachoElectronica != null)
            {
                var guiaDespachoElectronica = (PacioliGuiaDespachoElectronica)Doc;
                pacioliGuiaDespachoElectronica.MotivoGuia = guiaDespachoElectronica.MotivoGuia;
                pacioliGuiaDespachoElectronica.TipoDespacho = guiaDespachoElectronica.TipoDespacho;
            }
        }
    }
}
