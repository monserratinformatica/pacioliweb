﻿using PacioliSrv;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Net.Http.Headers;
using System.IO;
using PacioliWeb.Models;
using System.Net.Http.Formatting;
using System.Web.Http.Description;

namespace PacioliWeb.Controllers
{
    static class ComPacioliSingleton
    {
        private static IPacioliCOMObj srv;

        public static IPacioliCOMObj GetPacioliSrv(out string msg)
        {
            msg = "";
            if (srv == null)
            {
                msg = "Creando objeto";
                PacioliCOMObj obj = null;
                try
                {
                    obj = new PacioliCOMObj();
                }
                catch (Exception ex)
                {
                    msg = "Error creando ActiveX: " + ex.Message;

                }
                if (obj != null)
                {
                    string username = ConfigurationManager.AppSettings["Pacioli:username"].ToString();
                    string password = ConfigurationManager.AppSettings["Pacioli:password"].ToString();
                    string profile = ConfigurationManager.AppSettings["Pacioli:profile"].ToString();
                    if (obj.IniciarSesion(username, password, profile))
                    {
                        srv = obj;
                    }
                    else
                    {
                        msg = "No se pudo iniciar sesión - verifique path, username y password en Web.config";
                    }
                }
                else
                {
                    msg = "No se pudo crear ActiveX";
                }
            }
            return srv;
        }
    }
    public class DTEController : ApiController
    {
        protected virtual void AgregarEncabezado(PacioliDTE Doc, IPacioliDTE dte)
        {
            dte.Fecha = Doc.Fecha == DateTime.MinValue ? DateTime.Now : Doc.Fecha;
            dte.Receptor.Ciudad = Doc.Receptor.Ciudad;
            dte.Receptor.Comuna = Doc.Receptor.Comuna;
            dte.Receptor.Direccion = Doc.Receptor.Direccion;
            dte.Receptor.Giro = Doc.Receptor.Giro;
            dte.Receptor.RazonSocial = Doc.Receptor.RazonSocial;
            dte.Receptor.Rut = Doc.Receptor.RUT;
            dte.Receptor.Telefono = Doc.Receptor.Telefono;
            dte.Comentarios = Doc.Comentarios;
            dte.FormaDePago = Doc.FormaDePago;
        }

        protected static void AgregarLineasDetalle(PacioliDTE Doc, IPacioliDTE dte)
        {
            if (Doc.Detalle != null)
            {
                foreach (PacioliLineaDetalle item in Doc.Detalle)
                {
                    var nuevo = dte.NuevaLineaDetalle();
                    nuevo.TipoCodigo = item.TipoCodigo;
                    nuevo.ValorCodigo = item.ValorCodigo;
                    nuevo.NombreItem = item.NombreItem;
                    nuevo.PorcentajeDescuento = item.PorcentajeDescuento;
                    nuevo.Cantidad = item.Cantidad;
                    nuevo.Precio = item.Precio;
                    nuevo.Monto = item.Monto;
                    nuevo.UnidadMedida = item.UnidadMedida;
                    nuevo.Exento = item.Exento;
                    nuevo.DescripcionItem = item.DescripcionItem;
                    foreach (var imp in item.Impuestos)
                    {
                        var nuevoImpuesto = nuevo.NuevoImpuesto();
                        nuevoImpuesto.Codigo = imp.Codigo;
                        nuevoImpuesto.Tasa = imp.Tasa;
                    }

                    //Acá se permite enviar un arreglo de códigos, pero por ahora solo acepta 1 registro                
                    foreach (var cod in item.Codigos)
                    {
                        var nuevoCodigo = nuevo.NuevoCodigo();
                        nuevoCodigo.TipoCodigo = cod.TipoCodigo;
                        nuevoCodigo.ValorCodigo = cod.ValorCodigo;
                    }
                }
            }
        }

        /// <summary>
        /// Agrega documentos de referencia desde modelo hacia objeto interno
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="dte"></param>
        protected static void AgregarDocumentosReferencia(PacioliDTE doc, IPacioliDTE dte)
        {
            if (doc.DocumentosReferencia != null)
            {
                foreach (var docRef in doc.DocumentosReferencia)
                {
                    var nuevoDocRef = dte.NuevoDocumentoReferencia();
                    nuevoDocRef.TipoDocumento = docRef.TipoDocumento;
                    nuevoDocRef.Folio = docRef.Folio;
                    nuevoDocRef.RazonReferencia = docRef.RazonReferencia;
                    nuevoDocRef.FechaReferencia = docRef.FechaReferencia;
                }
            }
        }

        private HttpResponseMessage ObtenerPDF(int TipoDTE, long Folio, bool Cedible)
        {
            string msg;
            var obj = ComPacioliSingleton.GetPacioliSrv(out msg);
            string Mensaje;

            HttpResponseMessage result;
            // Esto graba en un archivo
            string fileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
            try
            {
                if (obj.ExportarPDF(TipoDTE, Folio, fileName, Cedible, out Mensaje) == 0)
                {
                    result = new HttpResponseMessage();
                    result.Content = new StreamContent(new FileStream(fileName, FileMode.Open));
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else
                {
                    result = new HttpResponseMessage(HttpStatusCode.NotFound);
                }
            }
            catch
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

            return result;
        }

        /// <summary>
        /// Obtiene muestra impresa cedible del documento 
        /// </summary>
        /// <param name="TipoDTE">Tipo de documento según códigos del SII (ej. 33)</param>
        /// <param name="Folio">Número de folio del documento</param>
        /// <returns>Representación impresa del documento en PDF</returns>
        [Route("api/dte/GetPDFCedible/{TipoDTE}/{Folio}")]
        public HttpResponseMessage GetPDFCedible(int TipoDTE, long Folio)
        {
            return ObtenerPDF(TipoDTE, Folio, true);
        }

        /// <summary>
        /// Obtiene documento emitido. El tipo de documento obtenido es determinado por el valor
        /// del header <code>Accept</code> en el Request.
        /// </summary>
        /// <remarks>
        /// <code>Accept: application/pdf</code> retorna la muestra impresa en formato PDF.
        /// <code>Accept: application/xml; charset=utf-8</code> retorna el documento en el formato XML del SII.
        /// </remarks>
        /// <param name="tipoDTE">Tipo de documento según códigos del SII (ej. 33)</param>
        /// <param name="folio">Número de folio del documento</param>
        /// <param name="rutEmisor">Rut del emisor del documento</param>
        /// <returns>Dte</returns> 
        [Route("api/dte/{tipoDTE}/{folio}/{rutEmisor?}")]
        [HttpGet]
        public HttpResponseMessage Get(int tipoDTE, long folio, int? rutEmisor = null)
        {
            try
            {
                IContentNegotiator negotiator = Configuration.Services.GetContentNegotiator();
                if (negotiator == null)
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = "No se pudo crear content negotiator" };

                ContentNegotiationResult result = negotiator.Negotiate(typeof(int), Request, Configuration.Formatters);
                if (result == null)
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = "No se pudo negociar content type " };

                if (result.MediaType.ToString() == "application/pdf")
                {
                    return GetPDF(tipoDTE, folio);
                }
                else if (result.MediaType.ToString().StartsWith("application/xml"))
                {
                    return GetXML(tipoDTE, folio);
                }
                else if (rutEmisor != null)
                {
                    return GetObjeto(tipoDTE, folio, (int)rutEmisor);
                }
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    ReasonPhrase = e.Message
                };
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }

        private HttpResponseMessage GetXML(int tipoDTE, long folio)
        {
            HttpResponseMessage result;
            string step = "";
            try
            {
                string Mensaje;
                var obj = ComPacioliSingleton.GetPacioliSrv(out Mensaje);
                step = "Obteniendo ActiveX";
                if (obj == null)
                {
                    step = "Retornando error";
                    result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                    result.Content.Headers.Add("X-Pacioli-Step", "Error obteniendo objeto ActiveX");
                    result.Content.Headers.Add("X-Pacioli-ErrMsg", Mensaje);
                    result.Content.Headers.Add("X-Pacioli-Parameters", string.Format("tipoDTE: {0} - folio: {1}", tipoDTE, folio));
                }
                else
                {
                    step = "Creando filename";
                    // Esto graba en un archivo
                    string fileName = Path.GetTempPath() + Guid.NewGuid().ToString() + ".xml";
                    step = string.Format("Exportando documento {0}, {1} en archivo {2} ", tipoDTE, folio, fileName);
                    if (obj.ExportarXML(tipoDTE, folio, fileName, out Mensaje) == 0)
                    {
                        step = string.Format("Objeto obtenido, retornando archivo {0}", fileName);
                        result = new HttpResponseMessage();
                        result.Content = new StreamContent(new FileStream(fileName, FileMode.Open));
                        
                        result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                    }
                    else
                    {
                        step = string.Format("Objeto obtenido, retornando not found - msg: {0}", Mensaje);
                        result = new HttpResponseMessage(HttpStatusCode.NotFound);
                        result.Content.Headers.Add("X-Pacioli-ErrMsg", Mensaje);
                        result.Content.Headers.Add("X-Pacioli-Parameters", string.Format("tipoDTE: {0} - folio: {1}", tipoDTE, folio));
                    }
                }
            }
            catch (Exception e)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = step };
                // result.Content.Headers.Add("X-Pacioli-ErrMsg", e.Message);
                // result.Content.Headers.Add("X-Pacioli-Parameters", string.Format("tipoDTE: {0} - folio: {1}", tipoDTE, folio));
            }
            return result;
        }

        /// <summary>
        /// Obtiene estado del documento en el Servicio de Impuestos Internos.
        /// </summary>
        /// <param name="tipoDTE">Tipo de documento según códigos del SII (ej. 33)</param>
        /// <param name="folio">Número de folio del documento</param>
        /// <returns>Objeto PacioliEstadoDTE con información del documento en el SII.</returns>
        [Route("api/dte/{tipoDTE}/{folio}/estado")]
        [HttpGet]
        [ResponseType(typeof(PacioliEstadoDTE))]
        public IHttpActionResult Estado(int tipoDTE, long folio)
        {
            string msg;
            var obj = ComPacioliSingleton.GetPacioliSrv(out msg);
            string mensaje;
            PacioliEstadoDTE pacioliEstadoDTE = new PacioliEstadoDTE();

            var estado = obj.VerificarDTE(tipoDTE, folio, out mensaje);

            if (estado == null && mensaje == null)
            {
                return NotFound();
            }
            else if (estado == null)
            {
                return BadRequest(mensaje);
            }

            pacioliEstadoDTE.ErrCode = estado.ErrCode;
            pacioliEstadoDTE.Estado = estado.Estado;
            pacioliEstadoDTE.GlosaErr = estado.GlosaErr;
            pacioliEstadoDTE.GlosaEstado = estado.GlosaEstado;
            pacioliEstadoDTE.NumAtencion = estado.NumAtencion;
            pacioliEstadoDTE.RawResponse = estado.RawResponse;

            return Ok(pacioliEstadoDTE);
        }

        /// <summary>
        /// Obtiene un listado con todos los documentos en un rango de fechas.
        /// </summary>
        /// <param name="desde">Desde que fecha se buscaran documentos</param>
        /// <param name="hasta">Hasta que fecha se buscaran documentos</param>
        /// <returns>Lista de Objeto PacioliResumenDocumento con información resumida de documentos.</returns>
        public IHttpActionResult Get(DateTime desde, DateTime hasta)
        {
            string msg = "";
            var obj = ComPacioliSingleton.GetPacioliSrv(out msg);
            if (obj == null)
                return InternalServerError();

            var listadoDtes = obj.ObtenerDocumentos(desde, hasta);
            PacioliListadoDocumentos listado = new PacioliListadoDocumentos();

            for (int i = 0; i < listadoDtes.Count; i++)
            {
                PacioliResumenDocumento documento = new PacioliResumenDocumento()
                {
                    Id = listadoDtes.get_Item(i).ID,
                    Folio = listadoDtes.get_Item(i).Folio,
                    TipoDocumento = listadoDtes.get_Item(i).TipoDocumento,
                    RutEmisor = listadoDtes.get_Item(i).RutEmisor,
                    FechaEmision = listadoDtes.get_Item(i).FechaEmision,
                    Total = listadoDtes.get_Item(i).Total,
                    Iva = listadoDtes.get_Item(i).IVA,
                };

                listado.Add(documento);
            }

            return Ok(listado);
        }

        /// <summary>
        /// Obtiene documento emitido. El tipo de documento obtenido es determinado por el request.
        /// </summary>
        /// <param name="tipoDTE">Tipo de documento según códigos del SII (ej. 33)</param>
        /// <param name="folio">Número de folio del documento</param>
        /// <param name="rutEmisor">Rut del emisor del documento</param>
        /// <returns>Objeto PacioliDTE</returns>        
        private HttpResponseMessage GetObjeto(int tipoDTE, long folio, int rutEmisor)
        {
            string mensaje;
            var obj = ComPacioliSingleton.GetPacioliSrv(out mensaje);
            if (obj == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No se pudo iniciar Pacioli: " + mensaje);

            var dte = obj.ObtenerDTE(folio, rutEmisor, tipoDTE, out mensaje);
            if (mensaje != null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, mensaje);
            }

            var pacioliDte = new PacioliDTE()
            {
                FormaDePago = dte.FormaDePago,
                Receptor = new PacioliEmpresa()
                {
                    RUT = dte.Receptor.Rut,
                    Giro = dte.Receptor.Giro,
                    RazonSocial = dte.Receptor.RazonSocial,
                    Telefono = dte.Receptor.Telefono,
                    Direccion = dte.Receptor.Direccion,
                    Ciudad = dte.Receptor.Ciudad,
                    Comuna = dte.Receptor.Comuna,
                },
                Emisor = new PacioliEmpresa()
                {
                    RUT = dte.Emisor.Rut,
                    Giro = dte.Emisor.Giro,
                    RazonSocial = dte.Emisor.RazonSocial,
                    Telefono = dte.Emisor.Telefono,
                    Direccion = dte.Emisor.Direccion,
                    Ciudad = dte.Emisor.Ciudad,
                    Comuna = dte.Emisor.Comuna,
                },

            };

            for (var i = 0; i < dte.Detalle.Count; i++)
            {
                var pacioliLineaDetalle = new PacioliLineaDetalle()
                {
                    NombreItem = dte.Detalle.Item[i].NombreItem,
                    Monto = dte.Detalle.Item[i].Monto,
                    Cantidad = dte.Detalle.Item[i].Cantidad,
                    PorcentajeDescuento = dte.Detalle.Item[i].PorcentajeDescuento,
                    Precio = dte.Detalle.Item[i].Precio,
                    TipoCodigo = dte.Detalle.Item[i].TipoCodigo,
                    UnidadMedida = dte.Detalle.Item[i].UnidadMedida,
                    ValorCodigo = dte.Detalle.Item[i].ValorCodigo,
                    Exento = dte.Detalle.Item[i].Exento,
                    DescripcionItem = dte.Detalle.Item[i].DescripcionItem,
                };

                for (var j = 0; j < dte.Detalle.Item[i].Impuestos.Count; j++)
                {
                    var pacioliImpuesto = new PacioliImpuesto()
                    {
                        Codigo = dte.Detalle.Item[i].Impuestos.Item[j].Codigo,
                        Tasa = dte.Detalle.Item[i].Impuestos.Item[j].Codigo,
                    };
                    pacioliLineaDetalle.Impuestos.Add(pacioliImpuesto);
                }
                pacioliDte.Detalle.Add(pacioliLineaDetalle);
            }

            for (int k = 0; k < dte.DocumentosReferencia.Count; k++)
            {
                var pacioliDocumentoReferencia = new PacioliDocumentoReferencia()
                {
                    TipoDocumento = dte.DocumentosReferencia.Item[k].TipoDocumento,
                    Folio = dte.DocumentosReferencia.Item[k].Folio,
                    FechaReferencia = dte.DocumentosReferencia.Item[k].FechaReferencia,
                    RazonReferencia = dte.DocumentosReferencia.Item[k].RazonReferencia,
                };
                pacioliDte.DocumentosReferencia.Add(pacioliDocumentoReferencia);
            }

            return Request.CreateResponse(HttpStatusCode.OK, pacioliDte);
        }

        /// <summary>
        /// Obtiene muestra impresa cedible del documento 
        /// </summary>
        /// <param name="TipoDTE">Tipo de documento según códigos del SII (ej. 33)</param>
        /// <param name="Folio">Número de folio del documento</param>
        /// <returns>Representación impresa del documento en PDF</returns>
        /// <seealso cref="GetPDF"/>
        [Obsolete("Use Get() instead")]
        [Route("api/dte/GetPDF/{TipoDTE}/{Folio}")]
        public HttpResponseMessage GetPDF(int TipoDTE, long Folio)
        {
            return ObtenerPDF(TipoDTE, Folio, false);
        }

    }
}
