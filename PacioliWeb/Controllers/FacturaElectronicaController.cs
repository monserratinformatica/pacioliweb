﻿using System;
using System.Net.Http;
using System.Web.Http;
using PacioliWeb.Models;
using Pacioli.Filters;
using System.Web.Http.Description;

namespace PacioliWeb.Controllers
{

    public class FacturaElectronicaController : DTEController
    {
        /// <summary>
        /// Emite una factura electrónica        
        /// </summary>       
        /// <param name="Doc">Objeto PacioliFacturaElectronica</param>
        /// <returns>ResultadoEnvio</returns>
        [ValidateModel]
        [ResponseType(typeof(ResultadoEnvio))]
        public IHttpActionResult Post(PacioliFacturaElectronica Doc)
        {
            if (Doc == null || Doc.Detalle.Count == 0)
                return BadRequest();
            string mensaje;
            var obj = ComPacioliSingleton.GetPacioliSrv(out mensaje);
            if (obj == null)
                return InternalServerError();

            var dte = obj.NuevaFacturaElectronica();

            AgregarEncabezado(Doc, dte);
            AgregarLineasDetalle(Doc, dte);
            AgregarDocumentosReferencia(Doc, dte);

            obj.EmitirDTE(dte, out mensaje);
            if (dte.Folio > 0)
            {
                var res = new ResultadoEnvio()
                {
                    Id = dte.NumeroEnvio,
                    Folio = dte.Folio
                };
                return Ok(res);
            }
            return BadRequest(mensaje);
        }

        /// <summary>
        /// Obtiene estado de una factura electronica emitida en el Servicio de Impuestos Internos.
        /// es un shortcut a Estado del controller DTE
        /// </summary>       
        /// <param name="folio">Número de folio del documento</param>
        /// <returns>Objeto PacioliEstadoDTE con información del documento en el SII.</returns>
        [Route("api/facturaelectronica/{Folio}/estado")]
        [HttpGet]
        public IHttpActionResult Estado(int folio)
        {
            return Estado(PacioliTipoDocumento.FacturaElectronica, folio);

        }

        /// <summary>
        /// Obtiene factura electrónica emitida. El tipo de documento obtenido es determinado por el request.
        /// es un shortcut al Get del controller DTE
        /// Puede retornar PDF si se especifica Accept: application/pdf en el Header
        /// </summary>       
        /// <param name="folio">Número de folio del documento</param>
        /// <param name="rutEmisor">Rut del emisor del documento</param>
        /// <returns>Dte</returns> 
        [Route("api/facturaelectronica/{folio}/{rutEmisor?}")]
        [HttpGet]
        public HttpResponseMessage Get(int folio, int rutEmisor = -1)
        {
            return Get(PacioliTipoDocumento.FacturaElectronica, folio, rutEmisor);
        }

        /// <summary>
        /// Método solo de ejemplo, En vez, utilizar Get especificando Accept: application/pdf en el Header
        /// </summary>       
        /// <param name="folio">Número de folio del documento</param>       
        /// <returns>PDF</returns> 
        [Obsolete("Use Get() instead")]
        [Route("api/facturaelectronica/getpdf/{folio}")]
        [HttpGet]
        public HttpResponseMessage GetPDF(int folio)
        {
            return GetPDF(PacioliTipoDocumento.FacturaElectronica, folio);
        }
    }
}
