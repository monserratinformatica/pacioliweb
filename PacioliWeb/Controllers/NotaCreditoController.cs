﻿using PacioliSrv;
using Pacioli.Filters;
using PacioliWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace PacioliWeb.Controllers
{
    public class NotaCreditoController : DTEController
    {
        /// <summary>
        /// Obtiene Nota de Credito electrónica emitida. El tipo de documento obtenido es determinado por el request.
        /// es un shortcut al Get del controller DTE        
        /// </summary>       
        /// <param name="folio">Número de folio del documento</param>
        /// <param name="rutEmisor">Rut del emisor del documento</param>
        /// <returns>Dte</returns> 
        [Route("api/notacredito/{folio}/{rutEmisor?}")]
        [HttpGet]
        public HttpResponseMessage Get(int folio, int rutEmisor = -1)
        {
            return Get(PacioliTipoDocumento.NotaCreditoElectronica, folio, rutEmisor);
        }

        /// <summary>Permite emitir una nota de crédito</summary>
        /// <param name="Doc">Nota de crédito a generar.</param>
        /// <remarks>FolioRef y TipoDocRef deben ser valores correspondientes a un documento ya emitido
        /// por Pacioli anteriormente, pues estos serán usados para obtener el documento original de la base de datos.</remarks>
        // POST: api/NotaCredito
        [ValidateModel]
        [ResponseType(typeof(ResultadoEnvio))]
        public IHttpActionResult Post(PacioliNotaCreditoElectronica Doc)
        {
            if (Doc == null)
                return BadRequest("No se pudo obtener documento desde request (JSON)");
            string msg;
            var obj = ComPacioliSingleton.GetPacioliSrv(out msg);
            var dte = obj.NuevaNotaCreditoElectronica();
            AgregarEncabezado(Doc, dte);

            AgregarLineasDetalle(Doc, dte);
            AgregarDocumentosReferencia(Doc, dte);

            dte.MotivoNota = Doc.MotivoNota;
            dte.TipoDocRef = Doc.TipoDocRef;
            dte.FolioRef = Doc.FolioRef;

            string mensaje;
            obj.EmitirDTE(dte, out mensaje);
            if (dte.Folio > 0)
            {
                var res = new ResultadoEnvio()
                {
                    Id = dte.NumeroEnvio,
                    Folio = dte.Folio
                };
                return Ok(res);
            }
            return BadRequest(mensaje);
        }

        [Route("api/notacreditoelectronica/{Folio}/estado")]
        [HttpGet]
        public IHttpActionResult Estado(int Folio)
        {
            return Estado(PacioliTipoDocumento.NotaCreditoElectronica, Folio);

        }

        protected override void AgregarEncabezado(PacioliDTE Doc, IPacioliDTE dte)
        {
            base.AgregarEncabezado(Doc, dte);

            if (dte is IPacioliNotaCreditoElectronica)
            {
                PacioliNotaCreditoElectronica NotaCreditoElectronica = (PacioliNotaCreditoElectronica)Doc;
                IPacioliNotaCreditoElectronica INotaCreditoElectronica = (IPacioliNotaCreditoElectronica)dte;

                INotaCreditoElectronica.FolioRef = NotaCreditoElectronica.FolioRef;
                INotaCreditoElectronica.MotivoNota = NotaCreditoElectronica.MotivoNota;
                INotaCreditoElectronica.TipoDocRef = NotaCreditoElectronica.TipoDocRef;
                INotaCreditoElectronica.FechaReferencia = NotaCreditoElectronica.FechaReferencia;
            }
        }
    }
}
