﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PacioliWeb.Validators
{
    public class TipoDocumentoValidoAttribute : ValidationAttribute
    {
        int[] _args = {
            33, // Factura Electrónica
            34, // Factura Exenta Electrónica
            56, // Nota de Débito Electrónica
            61, // Nota de Crédito Electrónica
            35, // Boleta (o comprobante fiscal)
            38, // Boleta Exenta
            39, // Boleta Electrónica
            41 // Boleta Exenta Electrónica
        };

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (_args.Contains((int)value))
            {
                return ValidationResult.Success;
            }

            return new ValidationResult("Tipo de Documento Inválido.");
        }
    }
}