﻿using PacioliWeb.Formatters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PacioliWeb
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            if ( ! ConfigurationManager.AppSettings["Pacioli:corsOrigin"].Equals(""))
            {
                var cors = new EnableCorsAttribute(ConfigurationManager.AppSettings["Pacioli:corsOrigin"].ToString(), "*", "*");
                config.EnableCors(cors);
            }            

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );            

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            config.Formatters.Add(new DtePdfFormatter()); 

        }
    }
}
