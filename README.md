# README #

Pacioli Web es un sitio Web API 2 que permite integrar tu aplicación web con Pacioli para emitir facturas electrónicas.

### ¿Cómo instalar Pacioli Web? ###

TODO

* Instalar Pacioli Facturación Electrónica normalmente en el mismo servidor
* Registrar objeto COM

```
#!cmd

cd "C:\Program Files (x86)\Pacioli Facturación"
pacioli.exe /regserver
```

* Descargar e instalar Pacioli Web
* Modificar Web.config con los datos a usar para conectar a Pacioli

# Como usar la API #

### Crear Factura ###
 POST request a http://tuhost/api/facturaelectronica


```
#!JSON

{
    "FormaDePago": "efectivo",
    "Detalle": [
        {
            "Cantidad": 1,
            "Exento": false,
            "Monto": 100,
            "NombreItem": "Itemdesdewebservice",
            "PorcentajeDescuento": 0,
            "Precio": 100,
            "TipoCodigo": "Interno",
            "UnidadMedida": "cajas",
            "ValorCodigo": "123"
        }
    ],
    "Receptor": {
        "Ciudad": "ciudad",
        "Comuna": "comuna",
        "Direccion": "dirección",
        "Giro": "girillo",
        "RUT": 11111111,
        "RazonSocial": "Empresa",
        "Telefono": "+5612345678"
    },
   
    "Comentarios": "probando comentarios",
    "DocumentosReferencia":[
       {
        "TipoDocumento": 33,
        "Folio": "5",
        "FechaReferencia": "2014-01-01T13:13:34.441Z",
        "RazonReferencia": "ref prueba"
       
       }
    ]
 
}
```

### Nota de Crédito ###
POST request a http://tuhost/api/notacredito

MotivoNota: 
**1**: Anula 
**‏ 2**:Corrige texto
**3**: Corrige montos‏


```
#!JSON
{
    "FormaDePago": "efectivo",
    "FolioRef": "110",
    "TipoDocRef": "33",
    "MotivoNota": "1",
    "Detalle": [
        {
            "Cantidad": 1,
            "Exento": false,
            "Monto": 100,
            "NombreItem": "Item desde web service",
            "PorcentajeDescuento": 0,
            "Precio": 100,
            "TipoCodigo": "Interno",
            "UnidadMedida": "cajas",
            "ValorCodigo": "123"
        }
    ],
    "Receptor": {
        "Ciudad": "ciudad",
        "Comuna": "comuna",
        "Direccion": "dirección",
        "Giro": "girillo",
        "RUT": 22222222,
        "RazonSocial": "Empresa"
    }
}

```

### Guía de Despacho ###
POST request a http://tuhost/api/guiadespachoelectronica
TipoDespacho: 
**1**: Despacho por cuenta del receptor del documento (cliente o vendedor en caso de Facturas de compra.) 
**2**: Despacho por cuenta del emisor a instalaciones del cliente
**3**: Despacho por cuenta del emisor a otras instalaciones (Ejemplo: entrega en Obra

MotivoGuia: 
**1**: Operacion Constituye Venta 
**2**: Ventas Por Efectuar 
**3**: Consignaciones 
**4**: Entrega Gratuita  
**5**: Traslados Internos
**6**: Otros Tras lados No Venta 
**7**: Guia Devolucion 
**8**: Traslado Para Exportacion No Venta 
**9**: Venta Para Exportacion

```
#!JSON

{
    "FormaDePago": "efectivo",
    "Detalle": [
        {
            "Cantidad": 1,
            "Exento": false,
            "Monto": 100,
            "NombreItem": "Itemdesdewebservice",
            "PorcentajeDescuento": 0,
            "Precio": 100,
            "TipoCodigo": "Interno",
            "UnidadMedida": "cajas",
            "ValorCodigo": "123"
        }
    ],
    "Receptor": {
        "Ciudad": "ciudad",
        "Comuna": "comuna",
        "Direccion": "dirección",
        "Giro": "girillo",
        "RUT": 22222222,
        "RazonSocial": "Empresa"
    },
    "Comentarios": "probando comentarios",
    "MotivoGuia": 1,
    "TipoDespacho": 3,
    "DocumentosReferencia": [
        {
            "TipoDocumento": 33,
            "Folio": "5",
            "FechaReferencia": "2014-01-01T13:13:34.441Z",
            "RazonReferencia": "ref prueba"
        }
    ]
}
```

### Verificar Estado de un documento en el SII ###

GET request a api/dte/{TipoDTE}/{Folio}/estado

### Obtener listado de Documentos ###
GET request a api/dte/?desde={fechaDesde}&hasta={fechaHasta}

### Obtener DTE ###
GET request a api/dte/{tipoDTE}/{folio}/{rutEmisor}

### Obtener DTE como PDF###
GET request a api/dte/{tipoDTE}/{folio}/{rutEmisor}
especificando **Accept: application/pdf** en el Header



Ver también: http://blog.monserratinformatica.cl/integracion-con-php/